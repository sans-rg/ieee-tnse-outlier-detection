import numpy as np
import pandas as pd
import cvxpy as cp
import math
import numpy.linalg as LA

class GraphLearning():
    '''
    This class implements the graph learning problem defined by Dong et al:
    "Learning Laplacian Matrix in Smooth Graph Signal Representations" using
    the CVXPY solver
    '''

    def __init__(self, data):
        '''
        Class initializer
        :param: data: NxP numpy matrix, where N is the number of signals and
                P is the number of network nodes
        '''
        self.train_X = np.copy(data)
        self.N = data.shape[0] # Number of signals
        self.P = data.shape[1] # Number of nodes
        self.L = cp.Variable((self.P, self.P), PSD=True) # Initialize variable to be PSD
        self.identity = np.identity(self.P)


    def __low_pass_filtering(self, X, alpha):
        '''
        This function applies a low-pass filering to the set of signals X
        :param X: PxN numpy matrix, set of graph signals
        :param alpha: float representing the amount of filtering
        :return: Y: PxN numpy matrix representing the filtered version of X
        '''
        Y = LA.inv(self.identity + (alpha*self.L.value))@X
        return Y

    def __objective_function(self, X, alpha, beta):
        '''
        Objective function of the graph learning problem
        :param X: PxN numpy matrix representing the set of graph signals
        :param alpha: float hyperparameter
        :param beta: float hyperparameter
        '''
        return alpha*(cp.trace(X.T@(self.L@X))) + beta*(cp.norm(self.L, 'fro')**2)

    def __constraints(self):
        '''
        This function implements the convex problem constraints
        '''
        cons = []
        cons.append(cp.trace(self.L) == self.P) # Trace constraint
        cons.append(cp.upper_tri(self.L) <=0 ) # Negative off-diagonal
        cons.append(self.L@np.ones(self.P) == 0) # Row sum equal to 0
        return cons

    def get_W(self):
        '''
        This function returns the learned Laplacian in a previous optimization,
        if learned
        :return L: PxP numpy matrix representing the learned Laplacian matrix
        '''
        return self.L.value

    def learn_graph(self, X, alpha, beta, max_iter=20):
        '''
        This method implements the alternate optimization procedure to
        learn the Laplacian matrix
        :param: X: PxN numpy matrix representing the set of graph signals
        :param: alpha: float hyperparameter
        :param: beta: float hyperparameter
        :param: max_iter: int maxumum number of iterations
        '''
        #construct the problem's constraints
        valid_L = self.__constraints()
        X_0 = np.copy(X) # initial solution
        objectives = np.zeros(max_iter)
        # iterative procedure
        for i in range(max_iter):
            obj = cp.Minimize(self.__objective_function(X, alpha, beta))
            prob = cp.Problem(obj, valid_L)
            # Solve convex problem using SCS solver
            prob.solve(verbose=True, solver=cp.SCS, max_iters=10000)
            # if problem is not feasible return 0s matrix
            if prob.status == cp.INFEASIBLE:
                print("INFEASIBLE PROBLEM")
                return np.zeros((self.P, self.P))
            # if problem is feasible filter the data X_0 using the learned laplacian
            X = self.__low_pass_filtering(X_0, alpha)
            # compute objective function value
            objectives[i] = LA.norm(X-X_0, 'fro')**2 + alpha*(np.trace(X.T@(self.L.value@X))) + beta*(LA.norm(self.L.value, 'fro')**2)
            # if decrease in objective function is less than 10e-4 return solution
            if i>=1 and (np.abs(objectives[i]-objectives[i-1])<(10e-4)):
                print("FINISHED AT ITERATION: " + str(i))
                print("Obj. difference: " + str(np.abs(objectives[i]-objectives[i-1])))
                return self.L.value
        print("OPTIMAL NOT FOUND")
        return np.zeros((self.P, self.P))
