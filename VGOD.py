###########################################################
##  VGOD outlier detection mechanism for                 ##
##     air pollution sensor networks.                    ##
##                             Ferrer-Cid et al. (2022)  ##
###########################################################
import numpy as np
import math
import os
import pandas as pd
from sklearn.metrics import confusion_matrix
from GraphVolterraFilter  import GraphVolterraFilter
from sklearn.preprocessing import StandardScaler
from GraphLearning import GraphLearning

class VGOD():

    @staticmethod
    def __data_perturbation(data, perc_P, perc_N, epsilon=0.5):
        '''
        This function perturbs a variable percentage of nodes of a subset of
        graph signals by adding zero-mean gaussian noise with epsilon
        standard deviations
        :param data: set of graph signals to perturb (numpy ndarray size N x P)
        :param perc_P: percentage of signals to perturb (float)
        :param perc_N: percentage of nodes to perturb (float)
        :return X_corrupted: corrupted version of the input data (numpy ndarray size N x P)
        '''
        N, P = data.shape
        P_perturbed = int(perc_P*N)
        N_perturbed = int(perc_N*P)
        # randomly choose the samples to perturb
        index_perturbed = np.random.choice(np.arange(N), P_perturbed, replace=False)
        X_corrupted = np.copy(data)
        for i in range(len(index_perturbed)):
            # randomly choose the number of nodes to perturb
            nodes_per = np.random.choice(np.arange(3, N_perturbed+1), 1)
            # randomly choose the nodes to perturb
            n_per = np.random.choice(np.arange(P), nodes_per, replace=False)
            X_corrupted[index_perturbed[i], n_per] = X_corrupted[index_perturbed[i], n_per] + np.random.normal(0.0, epsilon, nodes_per)
        return X_corrupted

    @staticmethod
    def __define_threshold(model, data, perc=1.0, delta=0.5, fpr=0.1, high_TH=1.5, method="SOLVER"):
        '''
        Function to compute the detection threshold based on the training mode,
        a desired false positive rate and the training data
        :param model: trained model (GraphVolterraFilter)
        :param data: training data (numpy ndarray)
        :param perc: percentage of samples to perturb (float)
        :param delta: perturbation magnitude (float)
        :param high_TH: largest threshold value to test (float)
        :return TH: computed threshold (float)
        '''
        N_TH = 150 # Number of thresholds to test
        REPETITIONS = 5 # Perform 5 repetitions
        ths = np.linspace(0.1, high_TH, N_TH) # Set of possible thresholds
        N, P = data.shape
        PERT = int(N*perc)
        # metrics
        fp_rates = np.zeros((REPETITIONS, N_TH))
        tp_rates = np.zeros((REPETITIONS, N_TH))
        # perform repetitions
        for i_rep in range(REPETITIONS):
            # compute indexes of signals to perturb
            indexes_pert = np.random.choice(np.arange(N), PERT, replace=False)
            # compute indexes of nodes to add the perturbation (only perturb one node per signal)
            nodes_pert = np.random.choice(np.arange(P), PERT, replace=True)
            # randomly select the sign of the perturbation
            sign = np.random.choice([1, -1], PERT, replace=True)
            out = np.zeros((N, P))
            out[indexes_pert, nodes_pert] = sign*delta
            data_cor = data + out
            # define the true outliers
            outliers = np.zeros(N)
            outliers[indexes_pert] = 1.0
            outliers_model = np.zeros((N, N_TH))

            # compute the reconstruted graph signals and residuals
            pred = np.apply_along_axis(lambda x: model.predict(x, method=method), 1, data_cor)
            res = np.max(np.abs(data_cor - pred), axis=1)
            # compute the detected outliers using every threshold
            for i_th in range(N_TH):
                indices_outliers = np.where(res>ths[i_th])[0]
                outliers_model[indices_outliers, i_th] = 1.0

            # compute the metrics for every possible threshold
            for i_fp in range(N_TH):
                tn, fp, fn, tp = confusion_matrix(np.abs(outliers), outliers_model[:, i_fp]).ravel()
                fp_rates[i_rep, i_fp] = (fp/(fp + tn))
                tp_rates[i_rep, i_fp] = (tp/(tp + fn))

        # average across repetitions
        fp_rates = np.mean(fp_rates, axis=0)
        tp_rates = np.mean(tp_rates, axis=0)
        # compute thresholds that meet the desired FPR
        res_TH = np.where(np.logical_and(fp_rates>(fpr-0.05), fp_rates<(fpr+0.05)))[0]
        # select best threshold
        if len(res_TH) == 0:
            raise Exception("No feasible configuration")
        best_TH = ths[res_TH[-1]]
        return best_TH

    @staticmethod
    def __learn_laplacian(data, alpha, beta):
        '''
        Learn the laplacian matrix using the model defined by Dong et al.
        :param data: standardized training graph signals (numpy ndarray N x P)
        :param alpha: model hyperparameter (float)
        :param beta: model hyperparameter (float)
        :return L: learned laplacian matrix (numpy ndarray PxP)
        '''
        gl = GraphLearning(data)
        L = gl.learn_graph(data.T, alpha, beta)
        return L

    def __init__(self, X_tr, alpha, beta, K=4, W=10, fpr=0.10, eps=0.5, sliding=False, size_train=1200, size_TH=300, method="SOLVER"):
        '''
        This function initializes the VGOD mechanism
        :param X_tr: training graph signals (numpy ndarray N x P)
        :param alpha: graph learning hyperparameter (float)
        :param beta: graph learning hyperparameter (float)
        :param K: filter depth (int)
        :param W: adaptive window size (int)
        :param fpr: required maximum false positive rate by the application (float)
        :param eps: perturbation to add when computing the threshold (float)
        :param sliding: whether the mechanism is applied using a sliding window or concatenating samples (bool)
        :param size_train: amount of training samples to use in the adapative phase (int)
        :param size_TH: number of samples to use when recomputing the threshold (int)
        :param method: solver method for GraphVolterraFilter (SOLVER or LS)
        '''
        self.X_tr = np.copy(X_tr) # trainign graph signals
        P, N = X_tr.shape
        self.N = N
        self.P = P
        self.K = K # filter depth
        self.W = W # window size
        self.fpr = fpr # maximum false positive rate
        self.eps = eps
        self.alpha = alpha
        self.beta = beta
        self.method = method
        # standardize training samples
        self.scaler = StandardScaler()
        self.scaler = self.scaler.fit(self.X_tr)
        self.X_tr_stand = self.scaler.transform(self.X_tr)
        # learn laplacian matrix
        self.L = self.__learn_laplacian(self.X_tr_stand, self.alpha, self.beta)
        # train Graph Volterra filter
        self.gsr_model = self.__train_gsr(self.X_tr_stand[-size_train:, :])
        self.iteration = 0 # Number of iterations performed adaptively
        # define the detection threshold
        self.TH = self.__define_threshold(self.gsr_model, self.X_tr_stand, perc=0.3, delta=self.eps, fpr=self.fpr, high_TH=1.5, method=self.method)
        self.sliding = sliding # whether to use sliding window or concatenation
        self.size_train = size_train
        self.size_TH = size_TH

    def __train_gsr(self, data):
        '''
        This function train the graph signal reconstruction model to denoise/correct
        perturbed samples
        :param data: training graph signals (numpy ndarray NxP)
        :return gsr_model: trained graph signal reconstruction model (GraphVolterraFilter)
        '''
        # perturb the training data
        perturbed_data = self.__data_perturbation(data, perc_P=1.0, perc_N=0.7)
        gsr_model = GraphVolterraFilter(self.L, data, self.K)
        gsr_model.fit(data, perturbed_data, method=self.method)
        return gsr_model

    def predict(self, x_new):
        '''
        This function predicts whether any of the graph signal nodes is an outlier
        :param x_new: new graph signal (numpy array size N)
        :return indicator: indicator vector, 1 indicates possible outlier (numpy array)
        '''
        # standardization of the new graph signal
        x_new_stand = self.scaler.transform(np.reshape(x_new, (1, -1)))[0]
        # residual of reconstruction
        residual = np.abs(x_new_stand-self.gsr_model.predict(x_new_stand, method=self.method))
        # thresholding
        ind = residual>self.TH
        indicator = np.zeros(self.N)
        indicator[ind] = 1.0
        return indicator

    def predict_adaptively(self, x_new):
        '''
        This function predicts whether any of the graph signal nodes is an outlier
        in an adaptive manner, updating the different models
        :param x_new: new graph signal (numpy array size N)
        :return indicator: indicator vector, 1 indicates possible outlier (numpy array)
        '''
        # standardization of the new graph signal
        x_new_stand = self.scaler.transform(np.reshape(x_new, (1, -1)))[0]
        # compute residual of the reconstruction
        residual = np.abs(x_new_stand-self.gsr_model.predict(x_new_stand, method=self.method))
        max_residual = np.max(residual)
        indicator = np.zeros(self.N)
        # check if any residual is below the threshold (non-outlier signal)
        if max_residual <= self.TH:
            # concatenate new graph signal to the current training set
            self.X_tr = np.append(self.X_tr, x_new.reshape((1, self.N)), axis=0)
            self.iteration = self.iteration + 1
            # if sliding window is activated most old signal is removed from training set
            if self.sliding == True:
                self.X_tr = np.delete(self.X_tr, 0, axis=0)

            # if iteration == W then the models need to be updated
            if self.iteration == self.W:
                # update standardization
                self.scaler = self.scaler.fit(self.X_tr)
                self.X_tr_stand = self.scaler.transform(self.X_tr)
                # update graph signal reconstruction model
                self.gsr_model = self.__train_gsr(self.X_tr_stand[-self.size_train:, :])
                # update threshold
                self.TH = self.__define_threshold(self.gsr_model, self.X_tr_stand[-self.size_TH:, :], perc=0.3, delta=self.eps, fpr=self.fpr, high_TH=1.5, method=self.method)
                self.iteration = 0

        else:
            # if outlier is detected return possible outliers
            ind = residual>self.TH
            indicator = np.zeros(self.N)
            indicator[ind] = 1.0

        return indicator
