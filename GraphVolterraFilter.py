import numpy as np
import numpy.linalg as LA
import cvxpy as cp
import math

class GraphVolterraFilter():
    '''
    This class implements the third order graph Volterra-based
    graph signal reconstruction model, using the CVXPY optimization
    library and solution by LeastSquares
    '''

    def __init__(self, A, X, K):
        '''
        This function initiaizes the class
        :param A: graph shift matrix (numpy ndarray)
        :param X: data matrix (numpy ndarray)
        :param K: model depth (int)
        '''
        self.A = np.copy(A) # Shift matrix
        self.X = np.copy(X) # Data matrix
        self.N = X.shape[1] # Number of nodes
        self.K = K          # Model depth
        self.N_training = X.shape[0] # # Training instances
        self.training_time = 0 # Solver time for last execution
        self.setup_time = 0    # Solver setup time for last execution
        self.result_opt = None # Solver result flag for last execution
        # Model learnable parameters
        self.H0 = cp.Variable(self.N)
        self.H1 = cp.Variable(self.K)
        self.H2 = cp.Variable((self.K, self.K))
        self.H3 = cp.Variable((self.K*self.K, self.K))
        # Model learnable parameters when training by LS (pseudoinverse)
        self.H_ls = None
        # Pre-computation of A matrix powers
        self.A_powers = np.zeros((self.K, self.N, self.N))
        for i_L in range(self.K):
            self.A_powers[i_L, :, :] = LA.matrix_power(self.A, i_L)


    def __loss_function(self, Y, uA, uAu, uAu3, pnorm):
        '''
        Loss function, as the sum of residuals vector norm
        :param Y: reference data (numpy ndarray)
        :param uA: first order interactions (numpy ndarray)
        :param uAu: second order interactions (numpy ndarray)
        :param uAu3: third order interactions (numpy ndarray)
        :param pnorm: loss function vector norm (int)
        :return loss
        '''
        # To speedup the computation the predictions are made node-wise
        return sum([cp.pnorm((Y[:, i] - ((self.H0[i]*np.ones(self.N_training)) + (self.H1@uA[:, :, i].T) + (cp.vec(self.H2)@uAu[:, :, i].T) + (cp.vec(self.H3)@uAu3[:, :, i].T))), pnorm)**pnorm for i in range(self.N)])



    def __fit_LS(self, Y, X):
        '''
        This function trains the model by least squares by pseudoinverse
        :param Y: reference data (numpy ndarray  size N_training x N)
        :param X: corrupted version of the reference data (numpy ndarray size N_training x N)
        '''
        N_tr = X.shape[0]
        X_ls = np.zeros(((self.K + self.N + (self.K*self.K) + (self.K*self.K*self.K)), N_tr*(self.N)))
        # iterate over all training instances to build a matrix
        # all nonlinear expansions are appended row-wise
        for i in range(N_tr):
            index = 0
            # Compute the 1st term -> L^ix
            for l in range(self.K):
                X_ls[l, (i*self.N):((i+1)*self.N)] = self.A_powers[l, :, :]@X[i, :]
            index = self.K
            end = index + (self.K*self.K)
            # Compute the second term (quadratic) -> (L^ix o L^jx)
            X_ls[index:end, (i*self.N):((i+1)*self.N)] = np.array([np.diagonal(self.A_powers[j, :, :]@np.einsum('i,j->ij', X[i, :], self.A_powers[i_L, :, :]@X[i, :])) for i_L in range(self.K) for j in range(self.K)])
            index = end
            end = index + (self.K*self.K*self.K)
            # Compute the third order term (cubic) -> (L^ix o (L^jx o L^kx))
            X_ls[index:end, (i*self.N):((i+1)*self.N)] = np.array([np.diagonal(np.einsum('i,j->ij', self.A_powers[i_L, :, :]@X[i, :], np.diagonal(np.einsum('i,j->ij', self.A_powers[i_L2, :, :]@X[i, :], self.A_powers[i_L3, :, :]@X[i, :])))) for i_L in range(self.K) for i_L2 in range(self.K) for i_L3 in range(self.K)])
            X_ls[end:, (i*self.N):((i+1)*self.N)] = np.identity(self.N)

        Y_ls = np.reshape(Y, (1, -1), order="C")
        # Compute model parameters by pseudoinverse
        self.H_ls = LA.pinv(X_ls.T)@Y_ls.T
        return

    def __predict_LS(self, x_new):
        '''
        This function reconstructs a graph signal given the x_new input signal
        :param x_new: graph signal to be reconstructed (numpy array size N)
        :return res: output graph signal
        '''
        X_ls = np.zeros(((self.K + self.N + (self.K*self.K) + (self.K*self.K*self.K)), self.N))
        index = 0
        i = 0
        for l in range(self.K):
            X_ls[l, (i*self.N):((i+1)*self.N)] = self.A_powers[l, :, :]@x_new
        index = self.K
        end = index + (self.K*self.K)
        X_ls[index:end, (i*self.N):((i+1)*self.N)] = np.array([np.diagonal(self.A_powers[j, :, :]@np.einsum('i,j->ij', x_new, self.A_powers[i_L, :, :]@x_new)) for i_L in range(self.K) for j in range(self.K)])
        index = end
        end = index + (self.K*self.K*self.K)
        X_ls[index:end, (i*self.N):((i+1)*self.N)] = np.array([np.diagonal(np.einsum('i,j->ij', self.A_powers[i_L, :, :]@x_new, np.diagonal(np.einsum('i,j->ij', self.A_powers[i_L2, :, :]@x_new, self.A_powers[i_L3, :, :]@x_new)))) for i_L in range(self.K) for i_L2 in range(self.K) for i_L3 in range(self.K)])
        X_ls[end:, (i*self.N):((i+1)*self.N)] = np.identity(self.N)
        x_predicted = self.H_ls.T@X_ls
        return np.reshape(x_predicted, x_new.shape)

    def __fit(self, Y, X):
        '''
        This function trains the model using the data using the CVXPY solver
        :param Y: reference data (numpy ndarray  size N_training x N)
        :param X: corrupted version of the reference data (numpy ndarray size N_training x N)
        :return
        '''
        # Precompute the first order, second order and third order interactions
        # Compute the 1st term -> L^ix
        Lxu = np.zeros((self.N_training, self.K, self.N))
        for i_s in range(self.N_training):
            Lxu[i_s, :, :] = np.array([self.A_powers[i, :, :]@X[i_s, :] for i in range(self.K)])

        # Compute the second term (quadratic) -> (L^ix o L^jx)
        Lxu2 = np.zeros((self.N_training, self.K*self.K, self.N))
        for i_s in range(self.N_training):
            Lxu2[i_s, :, :] = np.array([np.diagonal(np.einsum('i,j->ij', self.A_powers[i, :, :]@X[i_s, :], self.A_powers[j, :, :]@X[i_s, :])) for i in range(self.K) for j in range(self.K)])

        # Compute the third order term (cubic) -> (L^ix o (L^jx o L^kx))
        Lxu3 = np.zeros((self.N_training, self.K*self.K*self.K, self.N))
        for i_s in range(self.N_training):
            Lxu3[i_s, :, :] = np.array([np.diagonal(np.einsum('i,j->ij', self.A_powers[i_L, :, :]@X[i_s, :], np.diagonal(np.einsum('i,j->ij', self.A_powers[i_L2, :, :]@X[i_s, :], self.A_powers[i_L3, :, :]@X[i_s, :])))) for i_L in range(self.K) for i_L2 in range(self.K) for i_L3 in range(self.K)])


        # Define the probelm and solve
        obj = cp.Minimize(self.__loss_function(Y, Lxu, Lxu2, Lxu3, 2))
        prob = cp.Problem(obj)
        # SCS solver used, the number of iteratriions and the tolarance can be
        # adapted to the problem
        prob.solve(verbose=True, solver=cp.SCS, max_iters=5000, eps=1e-4)
        # if problem is not feasible return
        if prob.status == cp.INFEASIBLE:
            print("PROBLEM IS INFEASIBLE")
            return

        # Save the different execution statisttics
        self.training_time = prob.solver_stats.solve_time/1000.0 # Time in seconds
        self.result_opt = prob.status # Problem status
        self.setup_time = prob.solver_stats.setup_time/1000.0
        return

    def __predict(self, x_new):
        '''
        Given the trained modeñ a new sample x_new is reconstructed using
        a model trained with the CVXPY solver
        :param x_new: new graph signal (numpy ndarray size N)
        :return res: predicted graph signal (numpy ndarray size N)
        '''
        # Compute the 1st term -> L^ix
        Lxu = np.zeros((self.K, self.N))
        Lxu = np.array([self.A_powers[i, :, :]@x_new for i in range(self.K)])
        # Compute the second term (quadratic) -> diag(L^ix outer L^jx)
        Lxu2 = np.zeros((self.K*self.K, self.N))
        Lxu2 = np.array([np.diagonal(np.einsum('i,j->ij', self.A_powers[i, :, :]@x_new, self.A_powers[j, :, :]@x_new)) for i in range(self.K) for j in range(self.K)])
        # Compute the second term (cubic) -> diag(L^ix outer diag(L^jx outer L^kx)
        Lxu3 = np.zeros((self.K*self.K*self.K, self.N))
        Lxu3 = np.array([np.diagonal(np.einsum('i,j->ij', self.A_powers[i_L, :, :]@x_new, np.diagonal(np.einsum('i,j->ij', self.A_powers[i_L2, :, :]@x_new, self.A_powers[i_L3, :, :]@x_new)))) for i_L in range(self.K) for i_L2 in range(self.K) for i_L3 in range(self.K)])
        # compute output graph signal using vectorized representation
        res = self.H0 + (self.H1@Lxu) + (cp.vec(self.H2)@Lxu2) + (cp.vec(self.H3)@Lxu3)
        return res


    def fit(self, Y, X, method="SOLVER"):
        '''
        This functions trains the model using the Y,X data using the method (
        SOLVER or LS)
        :param Y: reference graph signals (numpy ndarray size N_training x N)
        :param X: perturbed graph signals (numpy ndarray size N_training x N)
        :param method: solving method CVXPY solver or Least Squares (SOLVER or LS)
        '''
        if method=="SOLVER":
            self.__fit(Y, X)
        elif method=="LS":
            self.__fit_LS(Y, X)
        else:
            raise Exception("Method not implemented, please select SOLVER or LS")
        return

    def predict(self, x_new, method="SOLVER"):
        '''
        This functions predicts an output graph signal using the sample X_new
        and the trained model using the method (SOLVER or LS)
        :param x_new: input graph signal (numpy array size N)
        :return res: output graph signal (numpy array size N)
        '''
        if method=="SOLVER":
            res = self.__predict(x_new).value
        elif method=="LS":
            res = self.__predict_LS(x_new)
        else:
            raise Exception("Method not implemented, please select SOLVER or LS")
        return res

    def save_model(self, dir, method="SOLVER"):
        '''
        This function saves the models parameters to files
        :param dir: desired path to write the files (str)
        :param method: method used to train the model
        '''
        if method=="SOLVER":
            self.__save_model(dir)
        elif method=="LS":
            self.__save_model_LS(dir)
        else:
            raise Exception("Method not implemented, please select SOLVER or LS")
        return
    def __save_model(self, dir):
        '''
        This function saves the models parameters trained using CVXPY to files
        :param dir: desired path to write the files (str)
        '''
        # Save the parameters in .npy format
        np.save(dir + "_H0", self.H0.value)
        np.save(dir + "_H1", self.H1.value)
        np.save(dir + "_H2", self.H2.value)
        np.save(dir + "_H3", self.H3.value)
        return
    def __save_model(self, dir):
        '''
        This function saves the models parameters trained by LS to files
        :param dir: desired path to write the files (str)
        '''
        # Save the parameters in .npy format
        np.save(dir + "_H_LS", self.H_ls)
        return

    def load_model(self, dir, method="SOLVER"):
        '''
        This function loads the models parameters to files
        :param dir: desired path to load the files (str)
        :param method: method used to train the model (str)
        '''
        if method=="SOLVER":
            self.__laod_model(dir)
        elif method=="LS":
            self.__load_model_LS(dir)
        else:
            raise Exception("Method not implemented, please select SOLVER or LS")
        return

    def __load_model(self, dir):
        '''
        This function retireves the model parameters trained by CVXPY from file
        :param dir: desired path to read the files (str)
        '''
        self.H0.value = np.load(dir + "_H0.npy")
        self.H1.value = np.load(dir + "_H1.npy")
        self.H2.value = np.load(dir + "_H2.npy")
        self.H3.value = np.load(dir + "_H3.npy")
        return
    def __load_model_LS(self, dir):
        '''
        This function retireves the model parameters trained by LS from file
        :param dir: desired path to read the files (str)
        '''
        self.H_ls = np.load(dir + "_H_LS.npy")

        return
