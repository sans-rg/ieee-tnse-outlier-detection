# VGOD outlier detection library used in: "Volterra Graph-Based Outlier Detection for Air Pollution Sensor Networks" paper #

Python library used in the elaboration of the paper: "Volterra Graph-Based Outlier Detection for Air Pollution Sensor Networks" published in the IEEE Transactions on Network Science and Engineering. The Python library implements the proposed Volterra Graph-Based Outlier Detection (VGOD) algorithm along with the Volterra-like raph signal reconstruction defined by Xiao et al. (2021) [2] and the graph learning optimization problem defined by Dong et al. (2016) [3]. Further details on the references are available in the repository.

### Files ###

* GraphVolterraFilter.py: This file contains the python class implementing the third order Volterra-based signal reconstruction using in the VGOD algorithm. The class contains the necessary functions to initiliza, train, save and load the model. The implementation is done using the CVXPY [1] package, so the optimization problem is solved using the CVXPY package with the Splitting Conic Solver (SCS). However, other solvers and tunning parameters can be used. The pseudoinverse solution to solve the least squares problem is also available.
* GraphLearning.py: This file contains a python class implementing the Laplacian learning problem defined by [3]. The optimization problem is implemented using the CVXPY python library.
* VGOD.py: This file contains a python class implementing the VGOD outlier detection algorithm for air pollution monitoring sensor networks. A static and an adaptive variant are available.
## References

* [1] Diamond, S., & Boyd, S. (2016). CVXPY: A Python-embedded modeling language for convex optimization. The Journal of Machine Learning Research, 17(1), 2909-2913.
* [2] Xiao, Z., Fang, H., & Wang, X. (2021). Distributed nonlinear polynomial graph filter and its output graph spectrum: Filter analysis and design. IEEE Transactions on Signal Processing, 69, 1-15.
* [3] Dong, X., Thanou, D., Frossard, P., & Vandergheynst, P. (2016). Learning Laplacian matrix in smooth graph signal representations. IEEE Transactions on Signal Processing, 64(23), 6160-6173.

